from django.shortcuts import render
from django.template import Context, Template
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def home_view(request): 
    #todo home_stats
    return render(request, 'home.html',{ 'something': 'something' })