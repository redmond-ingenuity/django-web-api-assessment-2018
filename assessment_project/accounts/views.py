from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import ( 
    authenticate,
    get_user_model,
    get_user,
    login,
    logout
)
from .models import Profile
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserLoginForm, UserEditProfileForm

# Create your views here.
def login_view(request):
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect('/')

    context = {
        'form': form,
    }
    return render(request, "login.html", context=context)


def register_view(request):
    next = request.GET.get('next')
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = User.objects.create_user(username=username, password=password)
        Profile.objects.create(user = user)
        # new_user = authenticate(username=username, password=password)
        login(request, authenticate(username=username, password=password))
        if next:
            return redirect(next)
        return redirect('/')

    context = {
        'form': form,
    }
    return render(request, "signup.html", context=context)

@login_required
def profile_view(request):
    user = request.user.profile
    context = {
        'user': user,
    }
    return render(request, "profile.html", context=context)

@login_required
def edit_profile_view(request):
    next = request.GET.get('next')
    form = UserEditProfileForm(request.POST or None, instance=request.user.profile)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.save()
        if next:
            return redirect(next)
        return redirect('/profile')

    context = {
        'form': form,
    }
    return render(request, "editprofile.html", context=context)

@login_required
def logout_view(request):
    logout(request)
    return redirect('/')