from django import forms
from django.contrib.auth import ( 
    authenticate,
    get_user_model
)
from .models import Profile
User = get_user_model()


class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError('User does not exist')
            if not user.check_password(password):
                raise forms.ValidationError('Incorrect password')
            if not user.is_active:
                raise forms.ValidationError('User is not active')
        return super(UserLoginForm, self).clean(*args, **kwargs)

class UserRegisterForm(forms.ModelForm):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('email')
        username_qs = User.objects.filter(username=username)
        if username_qs.exists():
            raise forms.ValidationError('This username is already taken')
        return super(UserRegisterForm, self).clean(*args, **kwargs)


#TodoEditProfile
class UserEditProfileForm(forms.ModelForm):
    first_name = forms.CharField(label='First Name')
    last_name = forms.CharField(label='Last Name')
    position = forms.CharField()
    management = forms.BooleanField()
    # photo = 
    birth_date = forms.DateField(widget=forms.SelectDateWidget)
    class Meta:
        model = Profile
        fields = [
            'first_name',
            'last_name',
            'position',
            'photo',
            'management',
            'birth_date'
        ]
    # def clean(self, *args, **kwargs):
    #     first_name = self.cleaned_data.get('first_name')
    #     last_name = self.cleaned_data.get('last_name')
    #     position = self.cleaned_data.get('position')
    #     management = self.cleaned_data.get('management')
    #     birth_date = self.cleaned_data.get('birth_date')
    #     return super(UserRegisterForm, self).clean(*args, **kwargs)