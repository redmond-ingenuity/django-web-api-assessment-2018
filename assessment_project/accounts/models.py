from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=30,null=True, blank=True, default='')
    last_name = models.CharField(max_length=30,null=True, blank=True, default='')
    birthdate = models.DateField(null=True, blank=True, default=datetime.date.today)
    photo = models.FileField(null=True, blank=True, default='test.jpg')
    position = models.CharField(max_length=30, default='none')
    management = models.BooleanField(null=False, blank=False, default=False)

    def __str__(self):
        showdetails = self.user.username + ' ' + self.first_name + ' ' + self.last_name + ' '
        return showdetails

    @classmethod
    def create_profile(cls):
        profile = cls()
        return profile

    def get_fields(self):
        return [(field.name, field.value_to_string(self), field) for field in Profile._meta.fields]

    def get_image_url(self):
        return self.photo.path

