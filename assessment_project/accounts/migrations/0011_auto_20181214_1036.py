# Generated by Django 2.1.4 on 2018-12-14 10:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_auto_20181214_1026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='photo',
            field=models.FileField(blank=True, default='test.jpg', null=True, upload_to=''),
        ),
    ]
